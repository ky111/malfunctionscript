USE [master]
GO
/****** Object:  Database [malfunctionDB]    Script Date: 31/03/2020 18:06:31 ******/
CREATE DATABASE [malfunctionDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'malfunctionDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\malfunctionDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'malfunctionDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\malfunctionDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [malfunctionDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [malfunctionDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [malfunctionDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [malfunctionDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [malfunctionDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [malfunctionDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [malfunctionDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [malfunctionDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [malfunctionDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [malfunctionDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [malfunctionDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [malfunctionDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [malfunctionDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [malfunctionDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [malfunctionDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [malfunctionDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [malfunctionDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [malfunctionDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [malfunctionDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [malfunctionDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [malfunctionDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [malfunctionDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [malfunctionDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [malfunctionDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [malfunctionDB] SET RECOVERY FULL 
GO
ALTER DATABASE [malfunctionDB] SET  MULTI_USER 
GO
ALTER DATABASE [malfunctionDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [malfunctionDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [malfunctionDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [malfunctionDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [malfunctionDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [malfunctionDB] SET QUERY_STORE = OFF
GO
USE [malfunctionDB]
GO
/****** Object:  Table [dbo].[Component]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Component](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[latitude] [float] NOT NULL,
	[longitude] [float] NOT NULL,
	[showName] [nvarchar](max) NULL,
 CONSTRAINT [PK_Component] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Factory]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Factory](
	[serviesName] [nvarchar](50) NOT NULL,
	[isOn] [bit] NULL,
 CONSTRAINT [PK_Factory] PRIMARY KEY CLUSTERED 
(
	[serviesName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Hirarchia]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hirarchia](
	[id] [int] NULL,
	[name] [nchar](3) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Layer]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Layer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
	[nameConvention] [nvarchar](max) NULL,
	[url] [nvarchar](max) NULL,
	[zoom] [int] NULL,
 CONSTRAINT [PK_Layer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LayerComponent]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LayerComponent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[layerId] [int] NOT NULL,
	[componentId] [int] NOT NULL,
 CONSTRAINT [PK_LayerComponent] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogMalfunction]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogMalfunction](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[malfunctionId] [int] NOT NULL,
	[responsibleId] [nvarchar](50) NOT NULL,
	[logMalfunctionStatusId] [int] NOT NULL,
	[statusDescription] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LogMalfunction] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogMalfunctionStatus]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogMalfunctionStatus](
	[id] [int] NOT NULL,
	[name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LogMalfunctionStatus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Malfunction]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Malfunction](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[componentId] [int] NOT NULL,
	[malfunctionStatusId] [int] NOT NULL,
	[malfunctionDescription] [nvarchar](max) NULL,
	[urgencyId] [int] NOT NULL,
 CONSTRAINT [PK_Malfunction] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[malfunctionStatus]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[malfunctionStatus](
	[id] [int] NOT NULL,
	[name] [nvarchar](max) NULL,
 CONSTRAINT [PK_malfunctionStatus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Responsible]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Responsible](
	[id] [nvarchar](50) NOT NULL,
	[roleId] [int] NOT NULL,
	[name] [nvarchar](max) NULL,
	[responsiblePassword] [nvarchar](max) NULL,
 CONSTRAINT [PK_Responsibles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[id] [int] NOT NULL,
	[name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Urgency]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Urgency](
	[id] [int] NOT NULL,
	[name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Urgency] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[id] [int] NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[password] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLocation]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLocation](
	[id] [nvarchar](50) NOT NULL,
	[userId] [int] NOT NULL,
	[componentLocationId] [int] NOT NULL,
 CONSTRAINT [PK_UserLocation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Component] ON 

INSERT [dbo].[Component] ([id], [name], [latitude], [longitude], [showName]) VALUES (1, N'cty_Tel-Aviv', 32.085294, 34.786949, N'Tel-Aviv')
INSERT [dbo].[Component] ([id], [name], [latitude], [longitude], [showName]) VALUES (2, N'cty_Jerusalem', 31.767407, 35.213127, N'Jerusalem')
INSERT [dbo].[Component] ([id], [name], [latitude], [longitude], [showName]) VALUES (3, N'stn_CentralStaition_cty_Jerusalem', 31.78857, 35.202287, N'CenteralStation')
INSERT [dbo].[Component] ([id], [name], [latitude], [longitude], [showName]) VALUES (4, N'stn_Hashlom_cty_Tel-Aviv', 32.073566, 34.792784, N'Hashalom')
INSERT [dbo].[Component] ([id], [name], [latitude], [longitude], [showName]) VALUES (5, N'flr_2_stn_Hashlom_cty_Tel-Aviv', 32.073566, 34.792784, N'floor 2')
INSERT [dbo].[Component] ([id], [name], [latitude], [longitude], [showName]) VALUES (6, N'component_cam_dir_N_num_675_flr_2_stn_Hashlom_cty_Tel-Aviv', 32.073566, 34.792784, N'camera number 675')
INSERT [dbo].[Component] ([id], [name], [latitude], [longitude], [showName]) VALUES (7, N'stn_Allenby_cty_Tel-Aviv', 32.067601, 34.769527, N'Allenby')
INSERT [dbo].[Component] ([id], [name], [latitude], [longitude], [showName]) VALUES (8, N'stn_Yefe-Nof_cty_Jerusalem', 31.773357, 35.185206, N'Yefe-Nof')
SET IDENTITY_INSERT [dbo].[Component] OFF
INSERT [dbo].[Factory] ([serviesName], [isOn]) VALUES (N'EsriTypeService', 0)
INSERT [dbo].[Factory] ([serviesName], [isOn]) VALUES (N'LocalTypeService', 1)
INSERT [dbo].[Hirarchia] ([id], [name]) VALUES (1, N'cty')
INSERT [dbo].[Hirarchia] ([id], [name]) VALUES (2, N'stn')
INSERT [dbo].[Hirarchia] ([id], [name]) VALUES (3, N'flr')
INSERT [dbo].[Hirarchia] ([id], [name]) VALUES (4, N'com')
INSERT [dbo].[Hirarchia] ([id], [name]) VALUES (5, N'dir')
INSERT [dbo].[Hirarchia] ([id], [name]) VALUES (6, N'num')
SET IDENTITY_INSERT [dbo].[Layer] ON 

INSERT [dbo].[Layer] ([id], [name], [nameConvention], [url], [zoom]) VALUES (1, N'city', N'cty', N'https://developers.google.com/maps/documentation/javascript/examples/full/images/parking_lot_maps.png', 8)
INSERT [dbo].[Layer] ([id], [name], [nameConvention], [url], [zoom]) VALUES (2, N'station', N'stn', N'https://developers.google.com/maps/documentation/javascript/examples/full/images/parking_lot_maps.png', 13)
INSERT [dbo].[Layer] ([id], [name], [nameConvention], [url], [zoom]) VALUES (3, N'floor', N'flr', N'https://i2.wp.com/yunu.design/wp-content/uploads/2018/11/1.jpg?https://developers.google.com/maps/documentation/javascript/examples/full/images/parking_lot_maps.png
', 18)
INSERT [dbo].[Layer] ([id], [name], [nameConvention], [url], [zoom]) VALUES (4, N'component', N'com', N'https://i2.wp.com/yunu.design/wp-content/uploads/2018/11/1.jpg?https://developers.google.com/maps/documentation/javascript/examples/full/images/parking_lot_maps.png
', NULL)
SET IDENTITY_INSERT [dbo].[Layer] OFF
SET IDENTITY_INSERT [dbo].[LogMalfunction] ON 

INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (2, 3, N'1', 1, N'the electric is malfunction')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (3, 8, N'1', 1, N'fvgbhnjm')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (4, 9, N'1', 1, N'dfghjk')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (5, 10, N'1', 1, N'try add new malfunction 11:34')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (6, 11, N'1', 1, N'try add new malfunction 11:34')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (7, 12, N'1', 1, N'kkkk')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (8, 13, N'1', 1, N'asdfghjkl;')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (9, 14, N'1', 1, N'tttttttttttttttttttttttttttttttttttttttttt')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (10, 15, N'1', 1, N'yuyu')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (11, 5, N'1', 1, N'i try')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (1005, 13, N'225225', 1, N'dfghjkl')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (1006, 1010, N'225225', 1, N'ehjkl')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (1011, 1010, N'1', 2, N'miri ordered')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (1012, 3, N'1', 2, N'miri ordered')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (1013, 3, N'1', 2, N'miri ordered')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (2005, 9, N'123456789', 2, N'yael ordered')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (2006, 2010, N'225225', 1, N'המצלמה נפלה')
INSERT [dbo].[LogMalfunction] ([id], [malfunctionId], [responsibleId], [logMalfunctionStatusId], [statusDescription]) VALUES (2007, 9, N'123456789', 2, N'yael ordered')
SET IDENTITY_INSERT [dbo].[LogMalfunction] OFF
INSERT [dbo].[LogMalfunctionStatus] ([id], [name]) VALUES (1, N'new')
INSERT [dbo].[LogMalfunctionStatus] ([id], [name]) VALUES (2, N'open')
INSERT [dbo].[LogMalfunctionStatus] ([id], [name]) VALUES (3, N'closed')
INSERT [dbo].[LogMalfunctionStatus] ([id], [name]) VALUES (4, N'hold')
SET IDENTITY_INSERT [dbo].[Malfunction] ON 

INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (3, 1, 1, N'the electric is malfunction', 1)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (4, 3, 1, N'is not working', 2)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (5, 2, 1, N'is malfunction', 3)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (6, 3, 2, N'ordered the responsible', 1)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (7, 3, 2, N'ordered the responsible', 1)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (8, 1, 1, N'fvgbhnjm', 1)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (9, 1, 1, N'dfghjk', 1)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (10, 1, 1, N'try add new malfunction 11:34', 1)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (11, 1, 1, N'try add new malfunction 11:34', 1)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (12, 7, 1, N'kkkk', 1)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (13, 2, 1, N'asdfghjkl;', 1)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (14, 2, 1, N'tttttttttttttttttttttttttttttttttttttttttt', 2)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (15, 2, 1, N'yuyu', 2)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (1010, 6, 1, N'ehjkl', 1)
INSERT [dbo].[Malfunction] ([id], [componentId], [malfunctionStatusId], [malfunctionDescription], [urgencyId]) VALUES (2010, 5, 1, N'המצלמה נפלה', 1)
SET IDENTITY_INSERT [dbo].[Malfunction] OFF
INSERT [dbo].[malfunctionStatus] ([id], [name]) VALUES (1, N'new')
INSERT [dbo].[malfunctionStatus] ([id], [name]) VALUES (2, N'open')
INSERT [dbo].[malfunctionStatus] ([id], [name]) VALUES (3, N'closed')
INSERT [dbo].[Responsible] ([id], [roleId], [name], [responsiblePassword]) VALUES (N'1', 1, N'miri', N'miri1234')
INSERT [dbo].[Responsible] ([id], [roleId], [name], [responsiblePassword]) VALUES (N'123456789', 2, N'yael', N'1234')
INSERT [dbo].[Responsible] ([id], [roleId], [name], [responsiblePassword]) VALUES (N'225225', 3, N'racheli', N'1234')
INSERT [dbo].[Responsible] ([id], [roleId], [name], [responsiblePassword]) VALUES (N'315211698', 2, N'ruth', N'1234')
INSERT [dbo].[Role] ([id], [name]) VALUES (1, N'חשמלאי')
INSERT [dbo].[Role] ([id], [name]) VALUES (2, N'מוקדנית')
INSERT [dbo].[Role] ([id], [name]) VALUES (3, N'מנהל')
INSERT [dbo].[Urgency] ([id], [name]) VALUES (1, N'Very urgent')
INSERT [dbo].[Urgency] ([id], [name]) VALUES (2, N'Urgent')
INSERT [dbo].[Urgency] ([id], [name]) VALUES (3, N'Not urgent')
ALTER TABLE [dbo].[Malfunction] ADD  CONSTRAINT [DF_Malfunction_urgencyId]  DEFAULT ((1)) FOR [urgencyId]
GO
ALTER TABLE [dbo].[LayerComponent]  WITH CHECK ADD  CONSTRAINT [FK_LayerComponent_Component] FOREIGN KEY([componentId])
REFERENCES [dbo].[Component] ([id])
GO
ALTER TABLE [dbo].[LayerComponent] CHECK CONSTRAINT [FK_LayerComponent_Component]
GO
ALTER TABLE [dbo].[LayerComponent]  WITH CHECK ADD  CONSTRAINT [FK_LayerComponent_Layer] FOREIGN KEY([layerId])
REFERENCES [dbo].[Layer] ([id])
GO
ALTER TABLE [dbo].[LayerComponent] CHECK CONSTRAINT [FK_LayerComponent_Layer]
GO
ALTER TABLE [dbo].[LogMalfunction]  WITH CHECK ADD  CONSTRAINT [FK_LogMalfunction_LogMalfunctionStatus] FOREIGN KEY([logMalfunctionStatusId])
REFERENCES [dbo].[LogMalfunctionStatus] ([id])
GO
ALTER TABLE [dbo].[LogMalfunction] CHECK CONSTRAINT [FK_LogMalfunction_LogMalfunctionStatus]
GO
ALTER TABLE [dbo].[LogMalfunction]  WITH CHECK ADD  CONSTRAINT [FK_LogMalfunction_Malfunction1] FOREIGN KEY([malfunctionId])
REFERENCES [dbo].[Malfunction] ([id])
GO
ALTER TABLE [dbo].[LogMalfunction] CHECK CONSTRAINT [FK_LogMalfunction_Malfunction1]
GO
ALTER TABLE [dbo].[LogMalfunction]  WITH CHECK ADD  CONSTRAINT [FK_LogMalfunction_Responsible] FOREIGN KEY([responsibleId])
REFERENCES [dbo].[Responsible] ([id])
GO
ALTER TABLE [dbo].[LogMalfunction] CHECK CONSTRAINT [FK_LogMalfunction_Responsible]
GO
ALTER TABLE [dbo].[Malfunction]  WITH CHECK ADD  CONSTRAINT [FK_Malfunction_Component1] FOREIGN KEY([componentId])
REFERENCES [dbo].[Component] ([id])
GO
ALTER TABLE [dbo].[Malfunction] CHECK CONSTRAINT [FK_Malfunction_Component1]
GO
ALTER TABLE [dbo].[Malfunction]  WITH CHECK ADD  CONSTRAINT [FK_Malfunction_malfunctionStatus1] FOREIGN KEY([malfunctionStatusId])
REFERENCES [dbo].[malfunctionStatus] ([id])
GO
ALTER TABLE [dbo].[Malfunction] CHECK CONSTRAINT [FK_Malfunction_malfunctionStatus1]
GO
ALTER TABLE [dbo].[Malfunction]  WITH CHECK ADD  CONSTRAINT [FK_Malfunction_Urgency1] FOREIGN KEY([urgencyId])
REFERENCES [dbo].[Urgency] ([id])
GO
ALTER TABLE [dbo].[Malfunction] CHECK CONSTRAINT [FK_Malfunction_Urgency1]
GO
ALTER TABLE [dbo].[Responsible]  WITH CHECK ADD  CONSTRAINT [FK_Responsible_Role] FOREIGN KEY([roleId])
REFERENCES [dbo].[Role] ([id])
GO
ALTER TABLE [dbo].[Responsible] CHECK CONSTRAINT [FK_Responsible_Role]
GO
ALTER TABLE [dbo].[UserLocation]  WITH CHECK ADD  CONSTRAINT [FK_UserLocation_Component] FOREIGN KEY([componentLocationId])
REFERENCES [dbo].[Component] ([id])
GO
ALTER TABLE [dbo].[UserLocation] CHECK CONSTRAINT [FK_UserLocation_Component]
GO
ALTER TABLE [dbo].[UserLocation]  WITH CHECK ADD  CONSTRAINT [FK_UserLocation_Responsible] FOREIGN KEY([id])
REFERENCES [dbo].[Responsible] ([id])
GO
ALTER TABLE [dbo].[UserLocation] CHECK CONSTRAINT [FK_UserLocation_Responsible]
GO
ALTER TABLE [dbo].[UserLocation]  WITH CHECK ADD  CONSTRAINT [FK_UserLocation_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[UserLocation] CHECK CONSTRAINT [FK_UserLocation_User]
GO
/****** Object:  StoredProcedure [dbo].[addComponent]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[addComponent]
@name nvarchar(max),
@latitude int,
@longitude int
as
insert into Component(name,latitude,longitude)
values(@name,@latitude,@longitude)

GO
/****** Object:  StoredProcedure [dbo].[addLayers]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[addLayers]
@name nvarchar(max),
@description nvarchar(max)
as
insert into Layer(name,description)
values(@name,@description)

GO
/****** Object:  StoredProcedure [dbo].[addLogMalfunction]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[addLogMalfunction]
@malfunctionId int,@responsibleId nvarchar(50),@logMalfunctionStatusId int,@statusDescription nvarchar(max)
as
insert into LogMalfunction(malfunctionId,responsibleId,logMalfunctionStatusId,statusDescription)
values (@malfunctionId,@responsibleId,@logMalfunctionStatusId,@statusDescription)

select SCOPE_IDENTITY()

GO
/****** Object:  StoredProcedure [dbo].[addLogMalfunctionStatus]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[addLogMalfunctionStatus]
@name nvarchar(max)
as
insert into LogMalfunctionStatus(name)
values(@name)

GO
/****** Object:  StoredProcedure [dbo].[addMalfunction]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[addMalfunction]

@componentId int,
@malfunctionDescription nvarchar(max),
@urgencyId int,

@responsibleId int
as
insert into Malfunction(componentId,malfunctionStatusId,malfunctionDescription,urgencyId)
values(@componentId,1,@malfunctionDescription,@urgencyId)
DECLARE @malfunctionId int
set @malfunctionId =SCOPE_IDENTITY()
exec addLogMalfunction @malfunctionId ,@responsibleId,1,@malfunctionDescription
return @malfunctionId

GO
/****** Object:  StoredProcedure [dbo].[addMalfunctionStatus]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[addMalfunctionStatus]
@name nvarchar(max)
as
insert into malfunctionStatus(name)
values(@name)

GO
/****** Object:  StoredProcedure [dbo].[addResponsible]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[addResponsible]
@id int,@role int,@name nvarchar(max),@responsiblePassword nvarchar(max)
as
insert into Responsible(id,name,responsiblePassword,roleId)
values(@id,@name,@responsiblePassword,@role)


GO
/****** Object:  StoredProcedure [dbo].[addRole]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[addRole]
@name nvarchar(max)
as
insert into Role(name)
values(@name)

GO
/****** Object:  StoredProcedure [dbo].[addUrgency]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[addUrgency]
@name nvarchar(max)
as
insert into Urgency(name)
values(@name)

GO
/****** Object:  StoredProcedure [dbo].[addUser]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[addUser]

@name nvarchar(max),
@idUser int,
@password nvarchar(max)
as
insert into UserWorker(name,idUser,password)
values(@name,@idUser,@password)

GO
/****** Object:  StoredProcedure [dbo].[checkUser]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[checkUser]
@name nvarchar(MAX),
@responsiblePassword nvarchar(MAX)
as
select * from Responsible
where name=@name and
responsiblePassword=@responsiblePassword

GO
/****** Object:  StoredProcedure [dbo].[getAllComponents]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getAllComponents]
as
select * from Component‏
GO
/****** Object:  StoredProcedure [dbo].[getAllLogMalfunctions]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getAllLogMalfunctions]
as
select LogMalfunction.id as LogMalfunctionDTO_id,Malfunction.id as MafunctionDTO_id,Component.id  as ComponentDTO_id,
Component.name as ComponentDTO_name,Component.latitude as ComponentDTO_latitude,Component.longitude as ComponentDTO_longitude,
malfunctionStatus.id as MalfunctionStatusDTO_id,malfunctionStatus.name as MalfunctionStatusDTO_name,
Malfunction.malfunctionDescription as MalfunctionDTO_malfunctionDescription,Urgency.id as UrgencyDTO_id,Urgency.name as UrgencyDTO_name,
Responsible.id as ResponsibleDTO_id,Role.id as RoleDTO_id,Role.name as RoleDTO_name,
Responsible.name as ResponsibleDTO_name,Responsible.responsiblePassword as ResponsibleDTO_responsiblePassword,
LogMalfunctionStatus.id as LogMalfunctionStatusDTO_id,LogMalfunctionStatus.name as LogMalfunctionStatusDTO_name,
statusDescription as LogMalfunctionDTO_statusDescription
from LogMalfunction
JOIN Malfunction  
on LogMalfunction.malfunctionId=Malfunction.id
join Component
on Malfunction.componentId=Component.id
join malfunctionStatus
on Malfunction.malfunctionStatusId=malfunctionStatus.id
join Urgency
on Malfunction.urgencyId=Urgency.id
join Responsible
on LogMalfunction.responsibleId=Responsible.id
join Role 
on Responsible.roleId=Role.id
join LogMalfunctionStatus
on LogMalfunction.logMalfunctionStatusId=LogMalfunctionStatus.id
GO
/****** Object:  StoredProcedure [dbo].[getAllStn]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getAllStn]
as
select * from Component
where name like 'stn%'

GO
/****** Object:  StoredProcedure [dbo].[getComponent]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getComponent]
@layerId int
as 
select * from Component
where id=(
select componentId from LayerComponent
where layerId=@layerId
)

GO
/****** Object:  StoredProcedure [dbo].[getComponentsByNamingConvention]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getComponentsByNamingConvention]
@first nvarchar(max),@contains nvarchar(max)
as
select * from Component
where name like @first+'%' and name like '%'+@contains+'%' 
GO
/****** Object:  StoredProcedure [dbo].[getHirarchiaList]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getHirarchiaList]
as
select * from Hirarchia
GO
/****** Object:  StoredProcedure [dbo].[getIsOn]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getIsOn]
as
select serviesName
from Factory
where isOn='True'
GO
/****** Object:  StoredProcedure [dbo].[getLayerByName]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getLayerByName]
@name varchar(max)
as
select * from Layer 
where @name=name

GO
/****** Object:  StoredProcedure [dbo].[getLayers]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getLayers]
as
select * from Layer

GO
/****** Object:  StoredProcedure [dbo].[getLogMalfunction]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getLogMalfunction]
as
select 
LogMalfunction.id,Component.name as ComponentName,
Responsible.id as ResponsibleId,
Responsible.name as ResponsibleName,
LogMalfunctionStatus.name as LogMalfunctionStatusName,
LogMalfunction.statusDescription,
malfunction.id as malfunctionId
from LogMalfunction
join Malfunction 
on LogMalfunction.malfunctionId=Malfunction.id
join Component
on Malfunction.componentId=Component.id
join Responsible 
on LogMalfunction.responsibleId=Responsible.id
join LogMalfunctionStatus
on LogMalfunction.logMalfunctionStatusId=LogMalfunctionStatus.id
GO
/****** Object:  StoredProcedure [dbo].[getLogMalfunctionStatus]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getLogMalfunctionStatus]
as 
select * from LogMalfunctionStatus
GO
/****** Object:  StoredProcedure [dbo].[getMalfunctionByComponentId]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getMalfunctionByComponentId]
@componentId int
as 
select Malfunction.id,Malfunction.componentId,Malfunction.malfunctionDescription,Component.name as componentName
from Malfunction 
join Component
on Malfunction.componentId=Component.id
where Malfunction.componentId=@componentId and Malfunction.malfunctionStatusId!=3
GO
/****** Object:  StoredProcedure [dbo].[getMalfunctions]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getMalfunctions]
as 
select * from Malfunction
GO
/****** Object:  StoredProcedure [dbo].[getMalfunctionStatus]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getMalfunctionStatus]
as 
select * from malfunctionStatus
GO
/****** Object:  StoredProcedure [dbo].[getNewMalfunctions]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getNewMalfunctions]
as
select m.id as malfunctionDTO_id, ms.name as malfunctionStatusDTO_name, c.name as componentDTO_name,
m.malfunctionDescription as malfunctionDTO_description, u.name as urgencyDTO_name
from Malfunction  m
join malfunctionStatus  ms
on m.malfunctionStatusId=ms.id
join Urgency  u
on m.urgencyId=u.id
join Component  c
on m.componentId=c.id
where m.malfunctionStatusId=1


--select m.id as MalfunctionDTO_id, m.malfunctionStatusId as malfunctionStatusDTO_id,
--m.urgencyId as urgencyDTO_id , u.name as urgencyDTO_name
--from Malfunction  m
--join malfunctionStatus  ms
--on m.malfunctionStatusId=ms.id
--join Urgency  u
--on m.urgencyId=u.id
--join Component  c
--on m.componentId=c.id
--where m.malfunctionStatusId=1

GO
/****** Object:  StoredProcedure [dbo].[getResponsible]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getResponsible]
as 
select Responsible.id, Responsible.name as name, Role.name as role
from Responsible
join Role on Responsible.roleId=Role.id
GO
/****** Object:  StoredProcedure [dbo].[getRoles]    Script Date: 31/03/2020 18:06:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getRoles]
as
select * from Role
GO
USE [master]
GO
ALTER DATABASE [malfunctionDB] SET  READ_WRITE 
GO
